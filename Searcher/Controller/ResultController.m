//
//  ResultController.m
//  Searcher
//
//  Created by user on 3/27/17.
//  Copyright © 2017 Developex. All rights reserved.
//

#import "ResultController.h"
#import "Result.h"
#import <ReactiveObjC/ReactiveObjC.h>


@interface ResultController ()

@property (nonatomic, nonnull) Result *result;

@end


typedef NSString *Status NS_EXTENSIBLE_STRING_ENUM;

static Status const StatusRunning   = @"Running";
static Status const StatusSuspended = @"Suspended";
static Status const StatusCanceling = @"Canceling";
static Status const StatusCompleted = @"Completed";


@implementation ResultController

#pragma mark - life cycle

- (nullable instancetype)initWithStatus:(Result *_Nonnull)result
{
    self = [super init];

    if (self) {
        [self bind];

        self->_result = [Result new];
    }
    return self;
}

- (void)bind
{
    @weakify(self);
    RAC(self, status) = [[RACSignal combineLatest:@[RACObserve(self, result.task.state),
                                                   RACObserve(self, result.task.error)]
                         reduce:^NSString *_Nonnull(NSNumber *_Nullable state,
                                                    NSError *_Nullable error) {
                             @strongify(self);

                             return
                             [self descriptionForState:state
                                                 error:error];
                         }]
                         startWith:
                         NSLocalizedString(StatusRunning,
                                           @"Task status description.")];

    RAC(self, progress) = [RACObserve(self, result.progress.fractionCompleted)
                           startWith:@(0.0)];
}

#pragma mark - private

- (NSString *_Nonnull)descriptionForState:(NSNumber *_Nullable)state
                                    error:(NSError *_Nullable)error
{
    if (error) {

        return [error localizedDescription];
    }
    return
    NSLocalizedString(( state
                       ? [@{@(NSURLSessionTaskStateRunning)   : StatusRunning,
                            @(NSURLSessionTaskStateSuspended) : StatusSuspended,
                            @(NSURLSessionTaskStateCanceling) : StatusCanceling,
                            @(NSURLSessionTaskStateCompleted) : StatusCompleted
                            } objectForKey:state]
                       : StatusRunning ),
                      @"Task status description.");
}

@end
