//
//  SearchController.m
//  Searcher
//
//  Created by user on 3/27/17.
//  Copyright © 2017 Developex. All rights reserved.
//

#import "SearchController.h"
#import "Search.h"
#import <ReactiveObjC/ReactiveObjC.h>


@interface SearchController ()

@property (nonatomic, nonnull) Search *search;

@end


@implementation SearchController

@synthesize results = _results;

#pragma mark - life cycle

- (nullable instancetype)init
{
    self = [super init];

    if (self) {
        [self bind];

        self->_search = [Search new];
    }
    return self;
}

- (void)bind
{
}

#pragma mark - properties

- (void)setResults:(NSArray<ResultController *> *)results
{
    self->_results = results ? [results copy] : @[];
}

- (NSArray<ResultController *> *)results
{
    if (! self->_results) {
        self->_results = @[];
    }
    return self->_results;
}

@end
