//
//  ResultController.h
//  Searcher
//
//  Created by user on 3/27/17.
//  Copyright © 2017 Developex. All rights reserved.
//

#import <Foundation/Foundation.h>


@class Result;


@interface ResultController : NSObject

// output
@property (copy, nonnull, readonly) NSString *status;
@property (copy, nonnull, readonly) NSNumber *progress;

// input
- (nullable instancetype)init NS_UNAVAILABLE;
- (nullable instancetype)initWithStatus:(Result *_Nonnull)result NS_DESIGNATED_INITIALIZER NS_REQUIRES_SUPER;

@end
