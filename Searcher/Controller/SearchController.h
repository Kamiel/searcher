//
//  SearchController.h
//  Searcher
//
//  Created by user on 3/27/17.
//  Copyright © 2017 Developex. All rights reserved.
//

#import <Foundation/Foundation.h>


@class ResultController;


@interface SearchController : NSObject

// output
@property (copy, nonatomic, null_resettable) NSArray<ResultController *> *results;

// input
@property (copy, nonatomic, nullable) NSString *startURL;
@property (copy, nonatomic, nullable) NSString *textToSearch;
@property (copy, nonatomic, nullable) NSString *jobsCount;
@property (copy, nonatomic, nullable) NSString *numberOfURL;

@end
