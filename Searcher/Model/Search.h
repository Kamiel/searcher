//
//  Search.h
//  Searcher
//
//  Created by user on 3/27/17.
//  Copyright © 2017 Developex. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Search : NSObject
/** Loading logic here */

@property (nonatomic, nonnull, readonly)       NSURL    *startURL;
@property (copy, nonatomic, nonnull, readonly) NSString *textToSearch;
@property (copy, nonatomic, nonnull, readonly) NSNumber *jobsCount;
@property (copy, nonatomic, nonnull, readonly) NSNumber *numberOfURL;

@end
