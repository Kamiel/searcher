//
//  Result.h
//  Searcher
//
//  Created by user on 3/27/17.
//  Copyright © 2017 Developex. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Result : NSObject
/** Just Value-Object, no logic here */

@property (nonnull, readonly) NSURLSessionTask *task;
@property (nonnull, readonly) NSProgress       *progress;

- (nullable instancetype)init NS_UNAVAILABLE;
- (nullable instancetype)initWithTask:(NSURLSessionTask *_Nonnull)task
                             progress:(NSProgress *_Nonnull)progress NS_DESIGNATED_INITIALIZER NS_REQUIRES_SUPER;

@end
