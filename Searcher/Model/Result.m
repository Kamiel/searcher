//
//  Result.m
//  Searcher
//
//  Created by user on 3/27/17.
//  Copyright © 2017 Developex. All rights reserved.
//

#import "Result.h"
#import <AFNetworking/AFNetworking.h>


@implementation Result

#pragma mark - life cycle

- (nullable instancetype)initWithTask:(NSURLSessionTask *_Nonnull)task
                             progress:(NSProgress *_Nonnull)progress
{
    self = [super init];

    if (self) {
        self->_progress = progress;
        self->_task     = task;
    }
    return self;
}

@end
