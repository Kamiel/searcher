//
//  Cell.m
//  Searcher
//
//  Created by user on 3/27/17.
//  Copyright © 2017 Developex. All rights reserved.
//

#import "Cell.h"
#import "../Controller/ResultController.h"
#import <ReactiveObjC/ReactiveObjC.h>
#import <ReactiveTableViewBinding/CEReactiveView.h>


@interface Cell () <CEReactiveView>

@property (weak, nonatomic, nullable) IBOutlet UIProgressView *progress;
@property (weak, nonatomic, nullable) IBOutlet UILabel        *status;

@end


@implementation Cell

- (void)awakeFromNib
{
    [super awakeFromNib];

    self.status.text = @"";
    self.progress.progress = 0.f;
}

#pragma mark - CEReactiveView

- (void)bindViewModel:(ResultController *)controller
{
    RAC(self, status.text) = [RACObserve(controller, status) takeUntil:
                              self.rac_prepareForReuseSignal];

    [[RACObserve(controller, progress) takeUntil:
      self.rac_prepareForReuseSignal]
     subscribeNext:
     ^(NSNumber *_Nonnull progress) {
         [self.progress setProgress:progress.floatValue
                           animated:YES];
     }];
}

@end
