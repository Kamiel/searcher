//
//  Table.m
//  Searcher
//
//  Created by user on 3/27/17.
//  Copyright © 2017 Developex. All rights reserved.
//

#import "Table.h"
#import "../Controller/SearchController.h"
#import "Cell.h"
#import <ReactiveObjC/ReactiveObjC.h>
#import <ReactiveTableViewBinding/CETableViewBindingHelper.h>


@interface Table ()

@property (strong, nonatomic, nonnull) IBOutlet UITextField *startURL;
@property (strong, nonatomic, nonnull) IBOutlet UITextField *textToSearch;
@property (strong, nonatomic, nonnull) IBOutlet UITextField *jobsCount;
@property (strong, nonatomic, nonnull) IBOutlet UITextField *numberOfURL;

@end


@implementation Table

#pragma mark - life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self bind];
    // actually, DI should be done in Control/Data Flow class.
    self.controller = [SearchController new];

    Class cellClass = [Cell class];
    [CETableViewBindingHelper bindingHelperForTableView:self.tableView
                                           sourceSignal:RACObserve(self, controller.results)
                                       selectionCommand:nil
                                      templateCellClass:cellClass
                                        reuseIdentifier:NSStringFromClass(cellClass)];
}

- (void)bind
{
    RAC(self, controller.startURL)     = RACObserve(self, startURL.text);
    RAC(self, controller.textToSearch) = RACObserve(self, textToSearch.text);
    RAC(self, controller.jobsCount)    = RACObserve(self, jobsCount.text);
    RAC(self, controller.numberOfURL)  = RACObserve(self, numberOfURL.text);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

    // TODO: cleanup search list.
}

@end
