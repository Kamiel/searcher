//
//  Table.h
//  Searcher
//
//  Created by user on 3/27/17.
//  Copyright © 2017 Developex. All rights reserved.
//

#import <UIKit/UIKit.h>


@class SearchController;


@interface Table : UITableViewController

@property (nonatomic, nonnull) SearchController *controller;

@end
